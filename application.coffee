View = require './view'
Router = require './router'
EventManager = require './events'
logger = require('./utils').logger

defaults =
  autolink: true
  redirect: false
  layout: 'layout'
  events: true

module.exports = Application = 

  start: (@options) ->
    @options[key] = value for key, value of defaults when not @options[key]?
    fn.call this for fn in @initializers

  initializers: [

    #
    # ### Autolinking
    # 
    # Autolinking captures all anchor tag click events, and if the link points
    # locally (i.e. within the app), it:
    # 
    #   * intelligently blocks the default behavior
    #   * navigates to the intended link using JS if possible
    #
    () ->
      if @options.autolink
        $(document).on 'click', 'a', (e) ->
          $target = $(e.currentTarget)
          href = $target.attr('href')
          if href and href?.indexOf('http') isnt 0
            e.preventDefault() unless $target.data('allow-default')
            (require './application').go(href) unless $target.data('prevent-navigate')? or href?.indexOf('#') > -1


    #
    # Render the layout view.
    #
    () ->
      @useLayout @options.layout


    #
    # Start the event manager
    #
    () ->
      if @options.events
        EventManager.start @options


    #
    # Setup the router and kick off history
    #
    () ->
      @router = new Router()
      @router.route url, view for url, view of @options.routes
      # Track the redirection URL
      if @options.redirect
        @redirect = @router.currentUrl()
      # Start history!
      Backbone.history.start pushState: @options.pushState, silent: (@options.redirect and @options.redirect isnt @redirect)
      (require './application').go @options.redirect if @options.redirect

  ]

  #
  # Loads the supplied URL
  #
  go: (url, options) ->
    (require './application').router.navigate url, _.extend(trigger: true, options)

  #
  # Loads the URL for the supplied resource
  #
  goto: (resource) ->
    @go resource.getResourceUrl()

  #
  # Render a top level view
  #
  use: (view) ->
    if view.layout? and view.layout isnt @layout.layoutId
      @useLayout view.layout, view.options.params
    @layout.add view, 'yield'

  useLayout: (id, params) ->
    Incoming = require('views/layouts/' + id)
    incoming = new Incoming params: params
    @layout?.destroy()
    @layout = incoming
    @layout.el.addClass 'forge-application'
    @options.$rootElement.append(@layout.el)

  #
  # Display an alert message
  #
  flash: (type, msg) ->
    alert "[#{type.toUpperCase()}] #{msg}"

  #
  # Logger
  #
  log: logger