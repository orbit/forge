String::slasherize = ->
  @transform '/'

String::dasherize = ->
  @transform '-'

String::transform = (separator) ->
  str = String(this)
  str = str.replace /[A-Z]/g, (c) -> "_" + c
  str = str.replace /[\/_\- ]/g, separator
  str = str.toLowerCase()
  str = str[1...] if str[0].match /[\/\-_ ]/
  return str

String::classify = ->
  str = String(this)
  str = str.replace /[A-Z]/g, (c) -> "_" + c
  str = str.replace /[\/_\- ][A-z]/g, (match) -> match[1].toUpperCase()
  str = str.replace /[\/_\- ]/g, ''
  return str

String::toMarker = ->
  "<script id='#{String(this)}'><\/script>"

String::slugify = ->
  str = String(this)
  str.replace /[^A-z0-9]/g, '-'

exports.logger = logger = (msg) ->
  console.log msg
logger.warn = (msg) ->
  console.warn msg
logger.error = (msg) ->
  console.error msg