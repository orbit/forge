# 
# # View
# 
# Views render a template with data into a containing DOM element. Top level
# Views (those listed as the handler's for routes) will be instantiated by
# Forge when their respective route is invoked.
# 
# ## Options
# 
#   * `params`: used by Forge to pass in variables matched against the URL
#     by the Router. Only found on top level views.
#   * `data`: rather than relying on the view's `prepare` function, if you
#     have the data this view needs on hand, you can pass it in directly.
#     Note: `prepare` will still be called even if you pass data in, and it
#     will override any data you supply.
#   * `template`: override the default template on an instance of a View.
#   * `tagName`: override the default tag that is created as this View's
#     container element. Note: this will have no effect if this View's
#     template causes unwrapping.
#     
module.exports = class View

  @uid: 0
  @_views: {}

  constructor: (@options = {}) ->

    @cid = "view#{++View.uid}"
    View._views[@cid] = this
    _.extend this, Backbone.Events

    @children = []
    @outlets = {}

    @el = $("<#{@options.tagName or @tagName or 'div'}>").addClass('forge-view').attr("id", @cid)

    @name = @options.name or @constructor.name

    templateName = @options.template or @template or @name.slasherize()
    @template = View.getTemplate templateName

    @initialize()

    @data = @options.data or {}
    @data.el = @el
    if @prepare.length isnt 2
      @prepare.call @data, @options
      @render()
    else
      @prepare.call @data, @options, @render

  initialize: ->

  nextTick: (fn) ->
    setTimeout fn, 0


  toString: ->
    @name or super

  # 
  # ## prepare
  # 
  # Defined here as an empty function. Your View subclasses should override
  # this method. Fetch any data needed to render the view inside this
  # function. `prepare` is called before rendering. If your `prepare` function
  # is asynchronous, just add a second parameter, `done`. This will be a
  # function you can once you have finished your async activity.
  # 
  prepare: (options) ->


  # 
  # ## render
  # 
  # Render's the view's template into it's container element using the view
  # `data`. Re-rendering destroys the existing DOM and child views, so always
  # try to `render` the smallest view possible. In general, you shouldn't be
  # calling render directly. This should be handled by the routing or changes
  # to the data.
  # 
  render: =>
    @destroyChildren()
    @el.html @template(@data).toString()
    @_processChildren()
    @_processOutlets()
    @_unwrap() unless @keepWrapper
    @postrender?()


  # 
  # ### $
  # 
  # Proxy for jQuery scoped selector. This shouldn't be used to often - if it
  # is, you might rethink breaking apart your view's functionality into child
  # views.
  # 
  $: (selector) -> @el.find(selector).add(@el.filter(selector))


  # 
  # ### add
  # 
  # Add a child view to this one. If no outlet is supplied, `add` does not
  # append the child to the DOM; it only creates the parent-child relationship
  # in memory.
  # 
  add: (view, outlet) =>
    @children.push view
    view.parent = this
    @listenTo view, 'destroy', @remove
    if outlet?
      if not @outlets[outlet]?
        require('./application').log.error "Outlet not found: #{outlet} in #{@template.id} (#{@name}##{@cid})"
      else
        @outlets[outlet].el.replaceWith view.el
        @outlets[outlet] = view


  get: (name) =>
    for child in @children
      if child.name is name
        return child
    return null


  # 
  # ### destroyChildren
  # 
  # Destroy's all children in this view.
  # 
  destroyChildren: =>
    children = @children.slice(0)
    while child = children.pop()
      child.destroy()


  # 
  # ### destroy
  # 
  # Destroy this view by removing it's element from the DOM, stopping all
  # event listeners, removing all children, and removing any link to a
  # possible parent. Unless you've retained another reference somewhere else,
  # this should free the view instance for garbage collection.
  # 
  destroy: =>
    @el.remove()
    @stopListening
    if @parent?
      i = @parent.children.indexOf this
      @parent.children.splice i, 1
    child.destroy() for child in @children


  # 
  # ## Unwrapping
  # 
  # In many cases, a template will have a single root element. In that case,
  # let's get rid of the useless View default element, and let that root
  # element do the job instead.
  _unwrap: =>
    if @el.children().length is 1
      inner = @el.children().first()
      inner.unwrap().addClass('forge-view')
      if not inner.attr('id')? then inner.attr('id', @cid)
      @el = inner


  # 
  # ## Child views
  # 
  # Child views are added in a two step process. First, during the template
  # rendering, outlets are inserted in the form of `<script>` tags. These tags
  # are linked to the child view instance by `cid`. `<script>` tags are used
  # because they do not visibly render.
  # 
  # The second pass happens after the template rendering, once the template's
  # HTML string has been hydrated into actual DOM. Then, we loop through the
  # children, find their marker elements, and replace those markers with the
  # view elements themselves.
  # 
  _processChildren: =>
    for child in @children
      @$("#" + child.cid).replaceWith child.el


  # 
  # ## Outlets
  # 
  # Outlets are handled similarly to child views, but rather than replacing
  # the marker elements, they are simply recorded for later use. Helpful for
  # keeping track of a DOM position without looking it up every time.
  # 
  _processOutlets: =>
    for outlet, el of @outlets
      @outlets[outlet] = el: @$("##{@cid}-#{outlet}-outlet")


  # 
  # ## View.getTemplate
  # 
  # Find the template matching the supplied name, or use the supplied
  # function. Injects data into the template renderer to ensure basic helper
  # variables are available: `parent` (the currently rendering view, used when
  # creating child views).
  # 
  # It also adds some debugging information, like the name of the template and
  # the template source itself, as properties on the resulting function.
  # 
  @getTemplate: (name) ->
    # Don't double wrap, just return it now and bail
    if _.isFunction(name) and name.id?
      return name
    fn = (data = {}) ->
      # make sure we don't pollute template scopes
      data = Handlebars.createFrame(data)
      data.parent = this
      # run the templating function if it's supplied directly
      if _.isFunction name
        name(data)
      # otherwise, if it's a string name for a precompiled template, use that
      else if window.JST[name]?
        window.JST[name](data).toString()
      else
        (require './application').log "Using blank template for #{@name} (#{@cid})"
        fn.id = null
        ""
    fn.id = name
    fn.src = window.JST[name]?.src
    return fn