# 
# # Handlebars Helpers
# 
# ## debug
# 
# Dumps all the information available at the current point in the template.
# Useful for understanding what's happening at that spot.
# 
Handlebars.registerHelper 'debug', (optionalValue, options) ->
  App.debug "Current Context - #{@parent.name} (#{@parent.cid})"
  App.debug this
  if options?
    App.debug "Value"
    App.debug optionalValue