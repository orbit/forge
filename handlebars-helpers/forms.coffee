# 
# # Handlebars Helpers
#
View = require '../view'


# 
# ## Form helpers
# 
# Use these helpers to create forms. There are two primary types of forms:
# standard and model-backed. For standard forms, you must supply a `submit`
# handler function, while model-backed forms automatically sync with the model
# they represent, and save that model on submission.
#


#
# ### form_for _model_
# 
# Creates form to represent the supplied model. The block is used as the form
# template, and the submit handler simply saves the model (the values should
# be already updated from data binding).
# 
Handlebars.registerHelper 'form_for', (model, options) ->
  form = new ModelForm(data: model, template: options.fn)
  @parent.add form
  return new Handlebars.SafeString form.cid.toMarker()


#
# ### input _field_ _attributes[hash]_
# 
# Creates an input field whose value is synced with the given field on this
# form's model. Only usable inside a model-backed form.
# 
Handlebars.registerHelper 'input', (model..., field, options) ->
  model = model[0] or @parent.data.model or @parent.data
  # setup the view
  input = new View(tagName: 'input')
  @parent.add input
  input.el.attr(name, val) for name, val of options.hash when name isnt 'class'
  input.el.addClass(options.hash['class']) if options.hash['class']?
  # write changes to the model
  input.onchange = (e) ->
    model.set(field, input.el.val(), forceUpdate: true)
    # check validation
    if model.isValid field
      input.trigger 'valid'
    else
      input.trigger 'invalid'
  # push changes to the DOM
  input.listenTo model, "change:#{field}", ->
    input.el.val model.get field
  return new Handlebars.SafeString input.cid.toMarker()


#
# ### textarea _field_ _attributes[hash]_
# 
# Creates an textarea field whose value is synced with the given field on this
# form's model. Only usable inside a model-backed form.
# 
Handlebars.registerHelper 'textarea', (model..., field, options) ->
  model = model[0] or @parent.data.model or @parent.data
  # setup the view
  textarea = new View(tagName: 'textarea')
  @parent.add textarea
  textarea.el.attr(name, val) for name, val of options.hash when name isnt 'class'
  textarea.el.addClass(options.hash['class']) if options.hash['class']?
  # write changes to the model
  textarea.onchange = (e) ->
    model.set(field, textarea.el.val(), forceUpdate: true)
    # check validation
    if model.isValid field
      textarea.trigger 'valid'
    else
      textarea.trigger 'invalid'
  # push changes to the DOM
  textarea.listenTo model, "change #{field}", ->
    textarea.el.val model.get field
  return new Handlebars.SafeString textarea.cid.toMarker()


