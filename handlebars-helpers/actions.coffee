# 
# # Handlebars Helpers
#

View = require '../view'

# 
# ## Event handlers
# 
# These helpers capture events in the DOM and trigger the given methods. For
# example,
# 
#     {{#onclick 'newComment'}}<button class="btn">Add a Comment</button>{{/onclick}}
# 
# Would trigger the `newComment` method on the containing view any time the
# `<button>` is clicked.
#


#
# ### onlick _target_ _action_
# 
# Creates a generic `View` using the block as the template. `target` is the
# object to which the `action` method is attached. Any click inside the block
# will trigger the action. If no `target` is specified, the containing view is
# used.
# 
Handlebars.registerHelper 'onclick', (action..., options) ->
  method = action.pop()
  target = action.pop() or @parent
  view = new View(tagName: options.hash.tagName, template: options.fn)
  view.onclick = (e) -> target[method]?()
  for attribute, value of options.hash when not (attribute in ['data', 'class'])
    view.el.attr(attribute, value)
  view.el.addClass(options.hash.class)
  @parent.add view
  return new Handlebars.SafeString view.cid.toMarker()