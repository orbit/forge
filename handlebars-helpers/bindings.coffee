TextBinding = require '../views/text-binding'

Handlebars.registerHelper 'bind-text', (model, field, options) ->
  view = new TextBinding(tagName: 'span', model: model, field: field)
  @parent.add view
  return new Handlebars.SafeString view.cid.toMarker()