# 
# # Handlebars Helpers
#


# 
# ## DOM helpers
# 
# These helpers aid in the creation of DOM elements. Many are used internally
# in Forge, but are registered as helpers in case you find the need to use
# them.
#


#
# ### make _tagName_ _attributes [hash]_
# 
# Creates an HTML tag with the supplied class names. If used in block form,
# the block is used as a template to render the contents of the tag. You can
# pass in data to be rendered with the block by specifying a `data=...`
# variable in the hash. Any other hash values specified are directly
# translated into HTML attributes. If `data` is a list, the block will be
# rendered once per array item.
# 
voidTags = [ 'area', 'base', 'br', 'col', 'command', 'embed', 'hr', 'img', 'input', 'keygen', 'link', 'meta', 'param', 'source', 'track', 'wbr' ]
Handlebars.registerHelper 'make', (tagName, options) ->

  # build the tag attributes
  attributes = ''
  if _.keys(options.hash).length > 0
    attributes += " #{a}=\"#{v}\"" for a, v of options.hash when a isnt 'data'

  # render any inner content (arrays become lists)
  if options.fn?
    if _.isArray options.hash.data
      body = options.fn(i) for i in options.hash.data
    else
      body = options.fn(options.hash.data or {})

  str = "<#{tagName}#{attributes}>"
  str += body or ''
  str += "</#{tagName}>" if not (tagName in voidTags)
  return new Handlebars.SafeString str




# 
# clickable
# 
# Creates an element and binds the click action all in one
# 
Handlebars.registerHelper 'clickable', (tag, action..., options) ->
  method = action.pop()
  target = action.pop() or @parent
  # cache the tag contents templating fn
  tagContents = options.fn
  # replace it with our wrapper, which takes the supplied tag inner content
  # and wraps it in a tag tag
  options.fn = => 
    Handlebars.helpers.make.call this, tag, _.extend {}, options, fn: tagContents
  # attach an onclick view to our tag wrapping function
  return Handlebars.helpers.onclick.call this, target, method, options


Handlebars.registerHelper 'button', (action..., options) ->
  method = action.pop()
  target = action.pop() or @parent
  return Handlebars.helpers.clickable.call this, 'button', target, method, options
Handlebars.registerHelper 'link', (action..., options) ->
  method = action.pop()
  target = action.pop() or @parent
  return Handlebars.helpers.clickable.call this, 'a', target, method, options
Handlebars.registerHelper 'linkTo', (resource, options) ->
  options.hash.href = resource.getResourceUrl()
  return Handlebars.helpers.make.call this, 'a', options


#
# ### outlet _name_
# 
# Creates `<script>` tag to use as a placeholder / marker. The id of the tag
# (to look it up later) combines the containing view's `cid` and the outlet
# name, ensuring that outlet names can never conflict (unless you specify two
# outlets of the same name in the same view).
# 
Handlebars.registerHelper 'outlet', (name) ->
  @parent.outlets[name] = null
  return new Handlebars.SafeString (@parent.cid + '-' + name + '-outlet').toMarker()

# 
# `yield` is a commonly used in views with a single outlet. We make it a direct
# DOM helper for convenience.
# 
Handlebars.registerHelper 'yield', (name) ->
  @parent.outlets.yield = null
  return new Handlebars.SafeString (@parent.cid + '-yield-outlet').toMarker()


#
# ### view _name_
# 
# Creates an instance of the supplied view name, and inserts it in this spot.
# The hash is passed directly through to the newly created view.
# 
Handlebars.registerHelper 'view', (path, options) ->
  View = require '../../views/' + path
  view = new View(options.hash)
  @parent.add view
  return new Handlebars.SafeString view.cid.toMarker()


#
# ### wrapWith _template name_
# 
# Wrap the supplied block with a template - no views are created, it's just
# like copy / pasting that template into this one
# 
Handlebars.registerHelper 'wrapWith', (templateName, options) ->
  inner = new Handlebars.SafeString options.fn(this)
  payload = _.extend {contents: inner}, this
  return new Handlebars.SafeString window.JST[templateName](payload).toString()