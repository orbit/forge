# 
# # Handlebars Helpers
#
List = require 'forge/views/list'

# 
# ## Ad-hoc lists
# 
# These helpers allow the creation of views and templates inline in a parent
# template. This can be especially useful for simply lists, where splitting
# everything out into separate files is overkill.
#


#
# ### foreach _items_
# 
# Creates a generic `List` view using the array supplied as the argument. The
# block for this helper becomes the item template for each generic `Item` view
# that is created.
# 
Handlebars.registerHelper 'foreach', (items, options) ->
  list = new List(_.extend {}, options.hash, data: items, itemTemplate: options.fn)
  @parent.add list
  return new Handlebars.SafeString list.cid.toMarker()