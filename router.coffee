# 
# # Router
# 
# Forge leverages the existing Backbone routing functionality, but replaces
# everything after the route is matched. Normally, Backbone routes are paired
# with a string which identifies the callback function that should be run when
# that route is invoked. In Forge, that string identifies which View handles
# that route. Forge uses this to instantiate the View at the top level of the
# application.
#
module.exports = class Router extends Backbone.Router

  currentUrl: -> window.location.pathname

  # Override route to setup views directly
  route: (route, view) ->
    # copied straight from Backbone
    if not _.isRegExp(route) then route = @_routeToRegExp(route)
    Backbone.history.route route, (fragment) =>
      # use the same extractParameters fn from Backbone
      args = @_extractParameters route, fragment
      # but rather than calling some router handler, setup an instance of the
      # view that this route maps to, and tell the global app to use this new
      # view at the top level
      View = require '/views/' + view
      (require './application').use new View(params: args)