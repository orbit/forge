View = require './view'

module.exports = EventManager = 

  events: [ 'click', 'submit', 'mouseup', 'mousedown', 'change', 'keydown' ]

  _listener: (e) ->
    unless e.forgeHandled?
      e.forgeHandled = true
      stop = false
      e.stopViewPropagation = ->
        stop = true
      view = View._views[e.currentTarget.id]
      while view and not stop
        view['on' + e.type]?(e)
        view.trigger e.type, e
        view = view.parent
      if e.type is 'submit' and e.target.tagName is 'FORM'
        e.preventDefault()
        return false

  start: ({$rootElement}) ->
    $rootElement.on type, '.forge-view', EventManager._listener for type in EventManager.events


