#
# # Form
#
# Forms automatically handle a lot of the bookkeeping that comes with form
# views. Submit events are automatically captured and routed, data is
# automatically serialized and deserialized, etc.
# 
# ## Options
# 
#   * `selectors`: a hash of selectors for retrieving various parts of the
#     form:
#     * `.form`: the `<form>` tag
#     * `.errors`: should select *all* error message containers
#     * `.formError`: used to display errors that don't pertain to specific
#       fields
#     * `.fieldError`: a function which takes a field name, and returns a
#       selector for that field's error message container
#     * `.submitButton`: the button to submit the form (also displays the
#       loading message)
#   * `validate`: a function that recieves the form data, and returns any
#     validation errors.
#
App = require 'forge/application'
View = require '../view'


defaultSelectors = 
  form           : 'form'
  errors         : '.error-block'
  formError      : '.error-block[data-for="form"]'
  fieldError     : (field) -> ".error-block[data-for='#{field}']"
  submitButton   : 'button[type="submit"], input[type="submit"]'


module.exports = class Form extends View


  constructor: (options) ->
    options.selectors = _.defaults options.selectors or {}, defaultSelectors
    super


  getButton: -> 
    @$(@options.selectors.submitButton)
  getForm: -> 
    @$(@options.selectors.form)


  #
  # Bind a default submit handler, with some conveniences
  # built in.
  #
  onsubmit: (e) ->
    $button = @getButton()
    # issue a warning if there is no submit handler on a form view
    if not @submit
      App.log.warn "No submit handler found for #{@name} (#{@cid})"
      @submit = _.identity
    # prevent the form from submitting normally
    e.preventDefault()
    # remove previous validation errors
    @clean()
    # serialize the form to JSON
    data = @serialize()
    # validate the data if asked
    if errors = @options.validate?() or @validate?()
      @fail errors
    else 
      # show the submitting state
      $button.button(@options.loadingText or @loadingText or 'loading ...')
      @submit data, => $button.button('reset')


  # 
  # ### fail
  # 
  # Shows validation errors. Can be called with a single string, or a hash of
  # fields and error messages.
  # 
  fail: (errors) =>
    # you can call fail with just an error string, to be handled as a form-wide error
    if typeof errors is 'string'
      errors = form: errors
    # if we can't find the form-wide error slot, trigger a global error alert
    if errors.form? and @$(@options.selectors.formError).length is 0
      App.flash 'error', errors.form
    # show each field's error message
    for field, err of errors
      @$(@options.selectors.fieldError(field)).text(err).addClass('active')
    # autofocus on the first errored field
    name = @$(".active#{@options.selectors.errors}").first().data('for')
    @$(":input[name='#{name}']").focus()
    @getButton().button('reset')


  # 
  # ### clean
  # 
  # Hide any validation errors.
  # 
  clean: ->
    @$(".active#{@options.selectors.errors or defaultSelectors.errors}")
     .removeClass('active')
     .text('')


  reset: ->
    @$(@options.selectors.form).get(0).reset()
    @clean()

  # 
  # ### serialize
  # 
  # Extract the data from all form elements.
  # 
  serialize: ->
    data = {}
    fields = @getForm().serializeArray()
    _.map fields, ({name, value}) ->
      if value is '' and not @allowEmptyStrings
        return
      if _.isArray(data[name])
        data[name].push value
      else if _.isUndefined(data[name])
        data[name] = value
      else
        data[name] = [data[name], value]
    data