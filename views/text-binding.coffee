# 
# # TextBinding
# 
# The `TextBinding` view is used internally to create a two-way databinding on
# an arbitrary piece of text in the DOM. Ember does this using metamorph.js
# and script tags for tracking positions. I took to the easy way out - span
# tags. The biggest downside is that this can mess with your styling, but as
# long as you are aware, it should be managable.
# 
# ## Options
# 
#   * `model`: the model to read from
#   * `field`: the name of the field on the given model to render
#          

View = require '../view'

module.exports = class TextBinding extends View

  tagName: 'span'

  constructor: ->
    super
    @listenTo @options.model, "change #{@options.field}", @render

  render: ->
    @el.text @options.model.get @options.field