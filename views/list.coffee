# 
# # List
# 
# The `List` view is a generic view class designed to render a collection. In
# addition to simply rendering each item, a `List` view automatically updates
# itself as the underlying collection does. List items are inserted at the
# `yield` outlet, or, if no `yield` outlet is found, at the end of the List
# view's containing element. 
# 
# ## Options
# 
#   * `data`: the collection to render
#   * `itemTemplate`: the template used to render each item in the list
#   * `itemView`: the view used to represent each item in the list. This
#     option is mutually exclusive with `itemTemplate`. `itemView` implies
#     assumes that the given view already has the proper template set, and it
#     will override `itemTemplate`.
#          

View = require '../view'
ListItem = require './list-item'

module.exports = class List extends View

  tagName: 'ul'

  render: ->
    super
    @el.addClass @options['class'] # FUGLY
    @listenTo @options.data, 'add', @addItem
    @options.data.each @addItem

  addItem: (item) =>
    # create the view instance from the supplied class, or the generic class
    # with the supplied template
    if @options.itemView?
      view = new @options.itemView(data: item)
    else
      view = new ListItem(data: item, template: @options.itemTemplate)
    # insert the view before the `yield` outlet, or, if there is no `yield`
    # outlet, at the end of the view
    if @outlets['yield']?
      @addBefore view, 'yield'
    else
      @add view
      @el.append view.el