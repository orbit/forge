# 
# # ListItem
# 
# The `ListItem` view is a generic view class designed to render a single
# model in a collection. In addition to simply rendering the model, a
# `ListItem` view automatically removes itself when the underlying model is
# destroyed. It also injects the index of the current model (within the
# containing collection) into the template data.
# 
# ## Options
# 
#   * `data`: the model to render
#   * `template`: the template used to render each item in the list
#          

View = require '../view'

module.exports = class ListItem extends View

  constructor: ->
    super
    @listenTo @options.data, 'destroy', @destroy

  load: (options) ->
    @index = options.data.collection?.indexOf(options.data) + 1